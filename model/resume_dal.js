var mysql  = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

/*
 create or replace view resume_view as
 select * from resume

 */

exports.getAll = function(callback) {
    var query = 'SELECT r.*, a.* FROM resume_ r join account a on a.account_id = r.account_id;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getAccounts = function(callback) {
    var query = 'SELECT * FROM account;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(resume_id, callback) {
    var query = 'call res_company_school_skill_getinfo(?)'
    var queryData = [resume_id];
    console.log(query);


    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.getByAccountId = function(account_id, callback) {
    var query = 'select * from account where account_id = ?;';
    var queryData = [account_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.getNameByResumeId = function(resume_id, callback) {
    var query = 'select resume_name, first_name, last_name from resume_ r left join account a on r.account_id = a.account_id where r.resume_id = ?;';
    var queryData = [resume_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
}

exports.getCompanyName = function(callback) {
    var query = 'select distinct c.company_id, company_name from resume_ r left join resume_company r_c on r.resume_id = r_c.resume_id left join company c on r_c.company_id = c.company_id where company_name is not null;';
    console.log(query);

    connection.query(query, function(err, result) {

        callback(err, result);
    });
}

exports.getCompanyNameByResumeId = function(resume_id, callback) {
    var query = 'select company_name from resume_ r left join resume_company r_c on r.resume_id = r_c.resume_id left join company c on r_c.company_id = c.company_id where r.resume_id = ?;';
    var queryData = [resume_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
}

exports.getSchoolName = function(callback) {
    var query = 'select distinct s.school_id, school_name from resume_ r left join resume_school r_s on r.resume_id = r_s.resume_id left join school s on r_s.school_id = s.school_id where school_name is not null;';
    console.log(query);

    connection.query(query, function(err, result) {

        callback(err, result);
    });

}

exports.getSchoolNameByResumeId = function(resume_id, callback) {
    var query = 'select school_name from resume_ r left join resume_school r_s on r.resume_id = r_s.resume_id left join school s on r_s.school_id = s.school_id where r.resume_id = ?;';
    var queryData = [resume_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });

}

exports.getSkillName = function(callback) {
    var query = 'select distinct k.skill_id, skill_name from resume_ r left join resume_skill r_k on r.resume_id = r_k.resume_id left join skill k on r_k.skill_id = k.skill_id where skill_name is not null;';
    console.log(query);

    connection.query(query, function(err, result) {

        callback(err, result);
    });

}

exports.getSkillNameByResumeId = function(resume_id, callback) {
    var query = 'select skill_name from resume_ r left join resume_skill r_k on r.resume_id = r_k.resume_id left join skill k on r_k.skill_id = k.skill_id where r.resume_id = ?;';
    var queryData = [resume_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });

}

/*exports.getResumeTables = function(company_name, school_name, skill_name, callback) {
    var query = 'select company_id, school_name, skill_name from resume_ r' +
        'join resume_company rc on rc.'
        'where account_id = ?;';
    var queryData = [company_name, school_name, skill_name];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};*/

exports.insert = function(params, callback) {

    // FIRST INSERT THE RESUME
    var query = 'CALL resume_insert(?, ?)';

    var queryData = [params.account_id, params.resume_name];

    connection.query(query, queryData, function(err, result){
        callback(err, result);
    });
};

exports.save_resume_companies = function(params, callback) {


    // FIRST SELECT THE RESUME_ID
    var query = 'select max(resume_id) max from resume_;';

    connection.query(query, function(err, result) {

        // THEN USE THE RESUME_ID RETURNED AS insertId AND THE SELECTED ADDRESS_IDs INTO COMPANY_ADDRESS
        var resume_id = result[0].max;

        // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
        var query = 'INSERT INTO resume_company(resume_id, company_id) VALUES ?;';

        // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
        var companyIdData = [];
        for(var i=0; i < params.company_id.length; i++) {
            companyIdData.push([resume_id, params.company_id[i]]);
        }

        // NOTE THE EXTRA [] AROUND companyAddressData
        connection.query(query, [companyIdData], function(err, result){
            callback(err, result);
        });
    });

};

exports.save_resume_schools = function(params, callback) {

    // FIRST SELECT THE RESUME_ID
    var query = 'select max(resume_id) max from resume_;';

    connection.query(query, function(err, result) {

        // THEN USE THE RESUME_ID RETURNED AS insertId AND THE SELECTED ADDRESS_IDs INTO COMPANY_ADDRESS
        var resume_id = result[0].max;

        // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
        var query = 'INSERT INTO resume_school(resume_id, school_id) VALUES ?;';

        // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
        var schoolIdData = [];
        for(var i=0; i < params.school_id.length; i++) {
            schoolIdData.push([resume_id, params.school_id[i]]);
        }

        // NOTE THE EXTRA [] AROUND companyAddressData
        connection.query(query, [schoolIdData], function(err, result){
            callback(err, result);
        });
    });
};

exports.save_resume_skills = function(params, callback) {

    // FIRST SELECT THE RESUME_ID
    var query = 'select max(resume_id) max from resume_;';

    connection.query(query, function(err, result) {

        // THEN USE THE RESUME_ID RETURNED AS insertId AND THE SELECTED ADDRESS_IDs INTO COMPANY_ADDRESS
        var resume_id = result[0].max;

        // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
        var query = 'INSERT INTO resume_skill(resume_id, skill_id) VALUES ?;';

        // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
        var skillIdData = [];
        for(var i=0; i < params.skill_id.length; i++) {
            skillIdData.push([resume_id, params.skill_id[i]]);
        }

        // NOTE THE EXTRA [] AROUND companyAddressData
        connection.query(query, [skillIdData], function(err, result){
            callback(err, result);
        });
    });
};

exports.delete = function(resume_id, callback) {
    var query = 'DELETE FROM resume_ WHERE resume_id = ?';
    var queryData = [resume_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};


exports.update = function(params, callback) {
    var query = 'UPDATE resume_ SET resume_name = ? WHERE resume_id = ?';
    var queryData = [params.resume_name, params.resume_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

/*  Stored procedure used in this example
     DROP PROCEDURE IF EXISTS resume_getinfo;

     DELIMITER //
     CREATE PROCEDURE resume_getinfo (resume_id int)
     BEGIN

     SELECT * FROM resume WHERE resume_id = _resume_id;


     END //
     DELIMITER ;

     # Call the Stored Procedure
     CALL resume_getinfo (4);

 */



exports.edit = function(resume_id, callback) {
    var query = 'CALL resume_getinfo(?)';
    var queryData = [resume_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

/*

 drop procedure if exists company_school_skill_getinfo;
 DELIMITER //
 create procedure company_school_skill_getinfo_by_resume_id(_resume_id int)
 begin
 SELECT r.*, a.*, school_name, skill_name, company_name FROM resume_ r
 join account a on a.account_id = r.account_id
 join resume_school r_s on r_s.resume_id = _resume_id
 join school s on r_s.school_id = s.school_id
 join resume_company r_c on r_c.resume_id = _resume_id
 join company c on r_c.company_id = c.company_id
 join resume_skill r_k on r_k.resume_id = _resume_id
 join skill k on r_k.skill_id = k.skill_id;

 end; //

 call company_school_skill_getinfo();
 */

exports.edit2 = function(callback) {
    var query = 'CALL company_school_skill_getinfo()';
    //var queryData = [account_id];

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};